use std::error::Error;
use std::convert::{ TryFrom, From };
use std::str::{ Utf8Error, from_utf8 };
use super::query_options::{ QueryOptions };
use super::method::{ HttpMethod, HttpMethodError };
use std::fmt::{ Display, Debug, Formatter, Result as FormatterResult };

#[derive(Debug)]
pub struct Request<'buffer> {
    path: &'buffer str,
    query_string: Option<QueryOptions<'buffer>>,
    method: HttpMethod
}

impl <'buffer> Request<'buffer> {

    pub fn path(&self) -> &str {
        &self.path
    }

    pub fn query_string(&self) -> Option<&QueryOptions> {
        self.query_string.as_ref()
    }

    pub fn method(&self) -> &HttpMethod {
        &self.method
    }

}

impl<'buffer> TryFrom<&'buffer[u8]> for Request<'buffer> {

    type Error = ParseError;

    fn try_from(value: &'buffer [u8]) -> Result<Request<'buffer>, Self::Error> {

        let request = from_utf8(value)?;
     
        let (method, request) = get_next_element(&request).ok_or(ParseError::InvalidRequest)?;
        let (mut path, request) = get_next_element(&request).ok_or(ParseError::InvalidRequest)?;
        let (protocol, _) = get_next_element(&request).ok_or(ParseError::InvalidRequest)?;

        if protocol != "HTTP/1.1" {
            return Err(ParseError::InvalidProtocol);
        }

        let mut query_string = None;
        let method = method.parse::<HttpMethod>()?;

        if let Some(i) = path.find("?") {
            query_string = Some(QueryOptions::from(&path[i + 1..]));
            path = &path[..i];
        }

        Ok(Self {
            path,
            query_string,
            method
        })

    }

}

pub enum ParseError {
    InvalidRequest,
    InvalidEncoding,
    InvalidProtocol,
    InvalidMethod
}

impl ParseError {
    fn get_message(&self) -> &str {
        match self {
            Self::InvalidRequest => "Invalid Request",
            Self::InvalidEncoding => "Invalid Encoding",
            Self::InvalidProtocol => "Invalid Protocol",
            Self::InvalidMethod => "Invalid Method"
        }
    }
}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FormatterResult {
        writeln!(f, "{}", &self.get_message())
    }
}

impl Debug for ParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FormatterResult {
        writeln!(f, "{}", &self.get_message())
    }
}

impl From<Utf8Error> for ParseError {
    fn from(_: Utf8Error) -> Self {
        Self::InvalidEncoding
    }
}

impl From<HttpMethodError> for ParseError {
    fn from(_: HttpMethodError) -> Self {
        Self::InvalidMethod
    }
}

impl Error for ParseError { }

fn get_next_element(value: &str) -> Option<(&str, &str)> {

    for (index, character) in value.chars().enumerate() {
        if character == ' ' || character == '\r' {
            return Some((&value[..index], &value[index + 1..]));
        }
    }

    None
    
}