use std::str::FromStr;

#[derive(Debug)]
pub enum HttpMethod {
    GET,
    POST,
    PUT,
    DELETE,
    OPTIONS,
    UPDATE
}

impl FromStr for HttpMethod {

    type Err = HttpMethodError;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        match value {
            "GET" => Ok(Self::GET),
            "POST" => Ok(Self::POST),
            "PUT" => Ok(Self::PUT),
            "DELETE" => Ok(Self::DELETE),
            "OPTIONS" => Ok(Self::OPTIONS),
            "UPDATE" => Ok(Self::UPDATE),
            _ => Err(HttpMethodError)
        }
    }

}

pub struct HttpMethodError;