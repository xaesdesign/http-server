use crate::http::HttpStatus;
use std::io::{ Write, Result as IOResult };

#[derive(Debug)]
pub struct Response {
    status_code: HttpStatus,
    body: Option<String>
}

impl Response {

    pub fn new(status_code: HttpStatus, body: Option<String>) -> Self {
        Response { status_code, body }
    }

    pub fn send(&self, stream: &mut impl Write) -> IOResult<()> {

        let body = match &self.body {
            Some(body) => body,
            None => ""
        };

        write!(stream, "HTTP/1.1 {} {}\r\n\r\n{}", self.status_code, self.status_code.reason_phrase(), body)

    }

}