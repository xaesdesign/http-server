use std::vec::Vec;
use std::fmt::Debug;
use std::convert::From;
use std::collections::HashMap;

#[derive(Debug)]
pub struct QueryOptions<'buffer> {
    data: HashMap<&'buffer str, Value<'buffer>>
}

#[derive(Debug)]
pub enum Value<'buffer> {
    SINGLE(&'buffer str),
    MULTIPLE(Vec<&'buffer str>)
}

impl <'buffer> QueryOptions<'buffer> {
    pub fn get(&self, key: &str) -> Option<&Value> {
        self.data.get(key)
    }
}

impl <'buffer> From<&'buffer str> for QueryOptions<'buffer> {
    fn from(value: &'buffer str) -> Self {

        let mut data = HashMap::<&'buffer str, Value<'buffer>>::new();

        for entry in value.split("&") {

            let mut key = entry;
            let mut value = "";

            if let Some(i) = entry.find("?") {
                key = &entry[..i];
                value = &entry[i + 1 ..];
            }

            data.entry(&key)
                .and_modify(|existing| { match existing {
                    Value::SINGLE(previous_value) => { *existing = Value::MULTIPLE(vec![previous_value, value]) },
                    Value::MULTIPLE(vector) => vector.push(value)
                }})
                .or_insert(Value::SINGLE(value));

        }

        QueryOptions { data }

    }
}