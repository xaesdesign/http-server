use std::fmt::{ Display, Formatter, Result as FormatterResult };

#[derive(Copy, Clone, Debug)]
pub enum HttpStatus {
    Ok = 200,
    BadRequest = 400,
    NotFound = 404
}

impl HttpStatus {
    pub fn reason_phrase(&self) -> &str {
        match self {
            Self::Ok => "Ok",
            Self::BadRequest => "Bad Request",
            Self::NotFound => "Not Found"
        }
    }
}

impl Display for HttpStatus {
    fn fmt(&self, f: &mut Formatter<'_>) -> FormatterResult {
        write!(f, "{}", *self as u16)
    }
}