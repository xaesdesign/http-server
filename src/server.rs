use std::io::Read;
use std::convert::TryFrom;
use std::net::TcpListener;
use crate::http::{ Request, Response, HttpStatus, ParseError };

pub struct Server {
    address: String
}

impl Server { 
    
    pub fn new(address: String) -> Self {
        Self { address }
    }

    pub fn run(self, mut handler: impl Handler) {

        println!("Listening for HTTP Connections on: {}.", self.address);
        println!("Public Path at: {}.", handler.public_path());
        
        let listener = TcpListener::bind(self.address).unwrap();

        loop {
            match listener.accept() {
                Ok((mut stream, _)) => {

                    let mut buffer = [0; 1024];
                    
                    match stream.read(&mut buffer) {
                        Ok(..) => {

                            let response = match Request::try_from(&buffer[..]) {
                                Ok(request) => handler.handle_request(&request),
                                Err(e) => handler.handle_error(&e)
                            };

                            if let Err(e) = response.send(&mut stream) {
                                println!("Error. Failed to send response: {}", e)
                            }

                        },
                        Err(e) => println!("Error. Failed to read the connection: {}", e)
                    }

                },
                Err(e) => println!("Error. Failed to establish a connection: {}", e)
            }
        }

    }

}

pub trait Handler {

    fn public_path(&self) -> &String;
    fn handle_request(&mut self, request: &Request) -> Response;

    fn handle_error(&mut self, error: &ParseError) -> Response {
        println!("Error. Failed to parse request: {}", error);
        Response::new(HttpStatus::BadRequest, None)
    }

}