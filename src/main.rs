#![allow(dead_code)]

use std::env;
use default_handler::DefaultHandler;

mod http;
mod server;
mod default_handler;

fn main() {

    // Getting Default Public Dir from env vars.

    let default_path = format!("{}/public", env!("CARGO_MANIFEST_DIR"));
    let public_path = env::var("PUBLIC_PATH").unwrap_or(default_path);

    // Creating and running HTTP Server.

    let server = server::Server::new("localhost:8000".to_string());
    server.run(DefaultHandler::new(public_path));

}
