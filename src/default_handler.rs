use super::server::Handler;
use super::http::{ Response, Request, HttpMethod, HttpStatus };
use std::fs::{ read_to_string, canonicalize };

pub struct DefaultHandler {
    public_path: String
}

impl DefaultHandler { 

    pub fn new(public_path: String) -> Self {
        Self { public_path }
    }

    pub fn read_file(&self, file_path: &str) -> Option<String> {

        let path = format!("{}/{}", self.public_path, file_path);

        match canonicalize(path) {
            Ok(path) => {
                if path.starts_with(&self.public_path) {
                    read_to_string(path).ok()
                } else {
                    println!("Directory Transversal Attack attempted at: {}", file_path);
                    None
                }
            },
            Err(_) => None
        }

    }

}

impl Handler for DefaultHandler {

    fn handle_request(&mut self, request: &Request) -> Response {
        match request.method() {
            HttpMethod::GET => match request.path() {
                "/" => Response::new(HttpStatus::Ok, self.read_file("/index.html")),
                "/hello" => Response::new(HttpStatus::Ok, Some(String::from("Hello!"))),
                path => {
                    match self.read_file(path) {
                        Some(content) => Response::new(HttpStatus::Ok, Some(content)),
                        None => Response::new(HttpStatus::NotFound, None)
                    }
                }
            },
            _ => Response::new(HttpStatus::NotFound, None)
        }
    }

    fn public_path(&self) -> &String {
        &self.public_path
    }

}